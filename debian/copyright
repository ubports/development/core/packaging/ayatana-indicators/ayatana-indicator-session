Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Ayatana Indicator Session
Upstream-Contact: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
Source: https://github.com/AyatanaIndicators/ayatana-indicator-session

Files: src/actions.c
 src/actions.h
 src/backend-dbus/actions.h
 src/backend-dbus/backend-dbus.c
 src/backend-dbus/backend-dbus.h
 src/backend-dbus/guest.c
 src/backend-dbus/guest.h
 src/backend-dbus/users.h
 src/backend-dbus/utils.c
 src/backend-dbus/utils.h
 src/backend.h
 src/guest.c
 src/guest.h
 src/main.c
 src/recoverable-problem.c
 src/recoverable-problem.h
 src/service.h
 src/users.c
 src/users.h
 tests/backend-dbus/gtest-mock-dbus-fixture.h
 tests/backend-dbus/mock-accounts.cc
 tests/backend-dbus/mock-accounts.h
 tests/backend-dbus/mock-display-manager-seat.cc
 tests/backend-dbus/mock-display-manager-seat.h
 tests/backend-dbus/mock-end-session-dialog.cc
 tests/backend-dbus/mock-end-session-dialog.h
 tests/backend-dbus/mock-login1-manager.cc
 tests/backend-dbus/mock-login1-manager.h
 tests/backend-dbus/mock-login1-seat.cc
 tests/backend-dbus/mock-login1-seat.h
 tests/backend-dbus/mock-lomiri-session.cc
 tests/backend-dbus/mock-lomiri-session.h
 tests/backend-dbus/mock-object.cc
 tests/backend-dbus/mock-object.h
 tests/backend-dbus/mock-screen-saver.cc
 tests/backend-dbus/mock-screen-saver.h
 tests/backend-dbus/mock-session-manager.cc
 tests/backend-dbus/mock-session-manager.h
 tests/backend-dbus/mock-user.cc
 tests/backend-dbus/mock-user.h
 tests/backend-dbus/mock-webcredentials.cc
 tests/backend-dbus/mock-webcredentials.h
 tests/backend-dbus/test-actions.cc
 tests/backend-dbus/test-guest.cc
 tests/backend-mock-actions.c
 tests/backend-mock-actions.h
 tests/backend-mock-guest.c
 tests/backend-mock-guest.h
 tests/backend-mock-users.c
 tests/backend-mock-users.h
 tests/backend-mock.c
 tests/backend-mock.h
 tests/gtest-dbus-fixture.h
 tests/test-service.cc
Copyright: 2012, Canonical Ltd.
  2013, Canonical Ltd.
  2014, Canonical Ltd.
License: GPL-3

Files: po/*.po
Copyright: 2010, THE PACKAGE'S COPYRIGHT HOLDER
  2017, THE PACKAGE'S COPYRIGHT HOLDER
  2009-2011, Rosetta Contributors and Canonical Ltd.
License: GPL-3
Comment:
 This file is distributed under the same license as the PACKAGE package.

Files: po/ayatana-indicator-session.pot
Copyright: YEAR, THE PACKAGE'S COPYRIGHT HOLDER
License: GPL-3
Comment:
 This file is distributed under the same license as the PACKAGE package.

Files: .build.yml
 .travis.yml
 AUTHORS
 ChangeLog
 CMakeLists.txt
 NEWS
 NEWS.Canonical
 README.md
 cmake/GdbusCodegen.cmake
 data/CMakeLists.txt
 data/ayatana-indicator-session.desktop.in
 data/ayatana-indicator-session.service.in
 data/org.ayatana.indicator.session
 data/org.ayatana.indicator.session.gschema.xml
 po/CMakeLists.txt
 po/LINGUAS
 po/POTFILES.in
 src/CMakeLists.txt
 src/backend-dbus/CMakeLists.txt
 src/backend-dbus/com.lomiri.Shell.Session.xml
 src/backend-dbus/org.ayatana.indicators.webcredentials.xml
 src/backend-dbus/org.freedesktop.Accounts.User.xml
 src/backend-dbus/org.freedesktop.Accounts.xml
 src/backend-dbus/org.freedesktop.DisplayManager.Seat.xml
 src/backend-dbus/org.freedesktop.login1.Manager.xml
 src/backend-dbus/org.freedesktop.login1.Seat.xml
 src/backend-dbus/org.freedesktop.login1.User.xml
 src/backend-dbus/org.gnome.ScreenSaver.xml
 src/backend-dbus/org.gnome.SessionManager.EndSessionDialog.xml
 src/backend-dbus/org.gnome.SessionManager.xml
 tests/CMakeLists.txt
 tests/ayatana-indicator-session.service.in
 tests/backend-dbus/CMakeLists.txt
 tests/manual
 tests/org.ayatana.indicator.session.backendmock.gschema.xml
 tests/org.ayatana.indicator.session.gschema.xml
 tests/org.gnome.desktop.lockdown.gschema.xml
 tests/org.gnome.settings-daemon.plugins.media-keys.gschema.xml
Copyright: 2012, Canonical Ltd.
  2013, Canonical Ltd.
  2014, Canonical Ltd.
  2017-2021, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2021, Robert Tari <robert@tari.in>
License: GPL-3
Comment:
 Assuming copyright holdership and licensing from other code files
 in this project.

Files: data/icons/16x16/actions/system-log-out.png
 data/icons/16x16/actions/system-restart.png
 data/icons/16x16/actions/system-shutdown.png
 data/icons/16x16/status/account-logged-in.png
 data/icons/22x22/actions/system-log-out.png
 data/icons/22x22/actions/system-restart.png
 data/icons/22x22/actions/system-shutdown.png
 data/icons/22x22/status/account-logged-in.png
 data/icons/24x24/actions/system-log-out.png
 data/icons/24x24/actions/system-restart.png
 data/icons/24x24/actions/system-shutdown.png
 data/icons/24x24/status/account-logged-in.png
 data/icons/32x32/actions/system-log-out.png
 data/icons/32x32/actions/system-restart.png
 data/icons/32x32/status/account-logged-in.png
 data/icons/scalable/actions/system-log-out.svg
 data/icons/scalable/actions/system-restart.svg
 data/icons/scalable/actions/system-shutdown.svg
 data/icons/scalable/status/account-logged-in.svg
Copyright: Jakub Steiner
  Lapo Calamandrei
  Kalle Persson
  Ricardo 'Rick' González
  Andreas Nilsson
License: GPL-2

Files: src/utils.c
 src/utils.h
 update-po.sh
 update-pot.sh
Copyright: 2017, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

Files: src/backend-dbus/actions.c
 src/backend-dbus/users.c
 src/service.c
 tests/backend-dbus/test-users.cc
Copyright: 2013, Canonical Ltd.
  2021, Robert Tari
  2021-2023, Robert Tari
  2023, Robert Tari
License: GPL-3

Files: INSTALL.md
Copyright: 2013, Canonical Ltd
  2017-2021, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

Files: debian/*
Copyright: 2011, Evgeni Golov <evgeni@debian.org>
           2009-2011, Ted Gould <ted@canonical.com>, Canonical Ltd.
           2009-2011, Sebastien Bacher <seb128@ubuntu.com>
           2009-2011, Ken VanDine <ken.vandine@canonical.com>
           2009-2011, Martin Pitt <martin.pitt@ubuntu.com>
           2017-2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
